FROM debian:11.4

# install packages
RUN apt-get update
RUN apt-get install -y build-essential git zlib1g zlib1g-dev libbz2-1.0 libbz2-dev libltdl7 libltdl-dev

# build hercules
RUN mkdir -p /usr/local/src
RUN cd /usr/local/src && git clone https://github.com/SDL-Hercules-390/hyperion.git
RUN cd /usr/local/src/hyperion && ./configure --prefix=/usr && make && make install
COPY entrypoint.sh /
